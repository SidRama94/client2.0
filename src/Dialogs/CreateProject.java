package Dialogs;

import gui.Call;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.net.URLDecoder;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingWorker;

import data.Project;
import data.ProjectCommunicationInstance;

public class CreateProject {
	public String pathDef = null;
	public static String path;
	public static JFrame frame;
	private JFileChooser chooser;
	private JTextField textField;
	private JButton btnNewButton;
	private JLabel lblNewLabel_2;
	private static JTextField textField_1;
	private JButton btnNewButton_1;
	private JLabel lblEnterProjectLocation;
	private static JTextField textField_2;
	public static int progress =0;
	public ProgressDialog  dialog;
	//private Workspace workspace;
	//public static Workspace workspacestatic;
	/**
	 * Launch the application.
	 */
	class ProgressDialog extends JPanel
	implements ActionListener, 
	PropertyChangeListener{
		
		 /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JProgressBar progressBar;
		private JFrame innerFrame;
		private JPanel contentPane;
		 public Task task;
		 private JLabel lblNewLabel;
		 
		 class Task extends SwingWorker<Void, Void> {

			@Override
			protected Void doInBackground() throws Exception {
				int progress=0;
				 setProgress(0);
				 System.out.println("calling creation");
				// CreateProject.projectCreationMethod();	
				 try {
					 	setProgress(10);
						path=URLDecoder.decode(textField_2.getText());
						String filePath=new File(path,URLDecoder.decode(textField_1.getText())).getAbsolutePath();
						File newProj=new File(path,URLDecoder.decode(textField_1.getText()));
						System.out.println("File path: "+filePath);
						if(!newProj.exists())
						{
							newProj.mkdir();
						}
						String first="first";
						setProgress(20);
						String projectName= URLDecoder.decode(textField_1.getText());
						Project p = new Project(projectName, path);
						//ProjectCommunicationInstance.launchInstance(p);
						File file = new File(filePath, ".nomedia");
						  if (file.createNewFile()){
						        System.out.println("File is created!");
						      }else{
						        System.out.println("File already exists.");
						      }
						 setProgress(50);
						Call.workspace.path=path;
						Call.workspace.name=projectName;
						setProgress(65);
						Call.workspace.enable();
						setProgress(75);
						
						Call.workspace.setupProject();	
						setProgress(100);
						setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
						frame.dispose();
						innerFrame.dispose();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				
				return null;
			}
			 
		 }
		 

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			
			if ("progress" == evt.getPropertyName()) {
	            int progress = (Integer) evt.getNewValue();
	            progressBar.setIndeterminate(false);
	            progressBar.setValue(progress);
			}
			
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
		}

		
		ProgressDialog() {

			innerFrame = new JFrame("Create Project");
			innerFrame.setBounds(100, 100, 528, 299);
			
			progressBar = new JProgressBar(0, 100);
	        progressBar.setValue(0);
	        progressBar.setStringPainted(true); 
	        progressBar.setIndeterminate(true);
	      
	        JPanel panel = new JPanel();
	        panel.setLayout(new BorderLayout(0, 0));
	        panel.add(progressBar);
	        panel.setSize(400, 30);
	        panel.setVisible(true);
	        panel.setOpaque(true);
	        innerFrame.setContentPane(panel);
	        
	        lblNewLabel = new JLabel("Creating project. Please wait....");
	        panel.add(lblNewLabel, BorderLayout.SOUTH);
	        innerFrame.setVisible(true);
	        System.out.println("Progress dialog created");
	        task = new Task();
	        task.addPropertyChangeListener(this);
	        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
	        task.execute();
		}
}
public void directoryChooser()
	{
		path=new DirectoryChooser().selectedfile;
		textField.setText(path);
	}
	public static void main(String[] args) {
		
		/*EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateProject window = new CreateProject();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});*/
		CreateProject window = new CreateProject();
		window.frame.setVisible(true);
		System.out.println("Exiting main");
		
	}

	/**
	 * Create the application.
	 */
	public CreateProject() {
		initialize();
	}
	
	public static void projectCreationMethod() {
		
			
	}
	
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 442, 280);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JLabel lblNewLabel = new JLabel("Create New Project");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel, 26, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel, 27, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblNewLabel);
		
		lblNewLabel_2 = new JLabel("Enter Project Name");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_2, 50, SpringLayout.SOUTH, lblNewLabel);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_2, 0, SpringLayout.WEST, lblNewLabel);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		frame.getContentPane().add(lblNewLabel_2);
		
		textField_1 = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textField_1, 0, SpringLayout.NORTH, lblNewLabel_2);
		springLayout.putConstraint(SpringLayout.WEST, textField_1, 46, SpringLayout.EAST, lblNewLabel_2);
		springLayout.putConstraint(SpringLayout.EAST, textField_1, 196, SpringLayout.EAST, lblNewLabel_2);
		
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		btnNewButton_1 = new JButton("Create");
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton_1, 0, SpringLayout.WEST, lblNewLabel);
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton_1, -28, SpringLayout.SOUTH, frame.getContentPane());
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField_1.getText().equals(""))
				{
					System.out.println("Text null");
					JOptionPane.showMessageDialog(null, "Enter the project name", "", JOptionPane.INFORMATION_MESSAGE);
				}
				else if(textField_2.getText().equals(""))
				{
					System.out.println("Path null");
					JOptionPane.showMessageDialog(null, "Enter the project location", "", JOptionPane.INFORMATION_MESSAGE);
				}
				else
				{

					File f=new File(textField_2.getText(),textField_1.getText());
					if(f.exists())
					{
						String str=textField_1.getText()+" already exisits at this location.";
						JOptionPane.showMessageDialog(null, str, "", JOptionPane.INFORMATION_MESSAGE);
					}
					else
						dialog = new ProgressDialog();
					
				System.out.println("Ended CreateProject");	
				
				
				}
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		frame.getContentPane().add(btnNewButton_1);
		
		lblEnterProjectLocation = new JLabel("Enter Project Location");
		springLayout.putConstraint(SpringLayout.NORTH, lblEnterProjectLocation, 25, SpringLayout.SOUTH, textField_1);
		springLayout.putConstraint(SpringLayout.WEST, lblEnterProjectLocation, 0, SpringLayout.WEST, lblNewLabel);
		lblEnterProjectLocation.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(lblEnterProjectLocation);
		
		textField_2 = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textField_2, 0, SpringLayout.NORTH, lblEnterProjectLocation);
		springLayout.putConstraint(SpringLayout.WEST, textField_2, 0, SpringLayout.WEST, textField_1);
		springLayout.putConstraint(SpringLayout.EAST, textField_2, 0, SpringLayout.EAST, textField_1);
		textField_2.setColumns(10);
		String Os = System.getProperty("os.name");
		if (Os.startsWith("Windows")) {
			pathDef = System.getProperty("user.home") + File.separatorChar + "My Documents";
		}
		else if (Os.startsWith("Linux")) {
			pathDef = System.getProperty("user.home") + File.separatorChar + "Documents";
		}
		
		else if (Os.startsWith("Mac")) {
			pathDef = System.getProperty("user.home") + File.separatorChar + "Documents";
		}
		textField_2.setText(pathDef);
		frame.getContentPane().add(textField_2);
		
		JButton btnNewButton_2 = new JButton("..");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				path=new DirectoryChooser(pathDef,"none").selectedfile;
				textField_2.setText(path);
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_2, -1, SpringLayout.NORTH, lblEnterProjectLocation);
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton_2, 6, SpringLayout.EAST, textField_2);
		frame.getContentPane().add(btnNewButton_2);
		
	}
}
