package Dialogs;

import gui.Call;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.Files;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingWorker;
import javax.swing.Timer;

import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.hslf.usermodel.SlideShow;


public class OpenPresentation {
	public JFrame frame;
	public String pathDef;
	public String path;
	private JButton btnNewButton_1;
	private JTextField textField_2;
	
	class ProgressDialog extends JPanel
	implements ActionListener, 
	PropertyChangeListener{
		
		 /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JProgressBar progressBar;
		private JFrame innerFrame;
		private JPanel contentPane;
		 public Task task;
		 private JLabel lblNewLabel;
		 
		 class Task extends SwingWorker<Void, Void> {

			@Override
			protected Void doInBackground() throws Exception {
				setProgress(0);
				path=textField_2.getText();
				System.out.println(path);
				File f=new File(Call.workspace.pptPath);
				f.delete();
				if(!path.contains(".pptx")&&(Call.workspace.pptPath.contains(".pptx")))
				{
					Call.workspace.pptPath=Call.workspace.pptPath.replace(".pptx", ".ppt");
				}
				else if(path.contains(".pptx")&&(!Call.workspace.pptPath.contains(".pptx")))
				{
					Call.workspace.pptPath=Call.workspace.pptPath.replace(".ppt", ".pptx");
				}
				copyFile(new File(path),new File(Call.workspace.pptPath));
				setProgress(15);
				setProgress(30);
				Call.workspace.repopulateProject();
				setProgress(50);
				setProgress(75);
				try {
					if(path.contains(".pptx"))
					{
						Call.workspace.currentPptx = new XMLSlideShow(new FileInputStream(Call.workspace.pptPath));
				    	Call.workspace.currentPptSlide = Call.workspace.currentPptx.getSlides();
					}
					else
						Call.workspace.currentPpt=new SlideShow(new FileInputStream(Call.workspace.pptPath));
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (java.io.IOException e2) {
					e2.printStackTrace();
				}
				setProgress(90);
				
				System.out.println("setup done");
				setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				setProgress(100);
				innerFrame.dispose();
				return null;
				
			}
			
		 }

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if ("progress" == evt.getPropertyName()) {
	            int progress = (Integer) evt.getNewValue();
	            progressBar.setIndeterminate(false);
	            progressBar.setValue(progress);
			}
			
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		ProgressDialog() {

			innerFrame = new JFrame("Import Presentation");
			innerFrame.setBounds(100, 100, 528, 299);
			
			progressBar = new JProgressBar(0, 100);
	        progressBar.setValue(0);
	        progressBar.setStringPainted(true); 
	        progressBar.setIndeterminate(true);
	      
	        JPanel panel = new JPanel();
	        panel.setLayout(new BorderLayout(0, 0));
	        panel.add(progressBar);
	        panel.setSize(400, 30);
	        panel.setVisible(true);
	        panel.setOpaque(true);
	        innerFrame.setContentPane(panel);
	        
	        lblNewLabel = new JLabel("Importing presentation. Please wait....");
	        panel.add(lblNewLabel, BorderLayout.SOUTH);
	        innerFrame.setVisible(true);
	        System.out.println("Progress dialog created");
	        task = new Task();
	        task.addPropertyChangeListener(this);
	        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
	        task.execute();
		}
	}
	
	
	
	
	
	
	
	
	
	
	

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					System.out.println("isnide");
					OpenPresentation window = new OpenPresentation();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public static void copyFile( File from, File to ){
	    try {
			Files.copy( from.toPath(), to.toPath() );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public OpenPresentation() {
		initialize();
	}
	
	public void initialize() {
		System.out.println("passed 1");
		frame = new JFrame();
		frame.setBounds(100, 100, 502, 280);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		JLabel lblNewLabel = new JLabel("Open Presentation");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		frame.getContentPane().add(lblNewLabel);
		
		textField_2 = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel, -3, SpringLayout.NORTH, textField_2);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel, -6, SpringLayout.WEST, textField_2);
		springLayout.putConstraint(SpringLayout.NORTH, textField_2, 73, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, textField_2, 197, SpringLayout.WEST, frame.getContentPane());

		textField_2.setColumns(10);
		String Os = System.getProperty("os.name");
		if (Os.startsWith("Windows")) {
			pathDef = System.getProperty("user.home") + File.separatorChar + "My Documents";
		}
		else if (Os.startsWith("Linux")) {
			pathDef = System.getProperty("user.home") + File.separatorChar + "Documents";
		}
		
		else if (Os.startsWith("Mac")) {
			pathDef = System.getProperty("user.home") + File.separatorChar + "Documents";
		}
		textField_2.setText(pathDef);
		frame.getContentPane().add(textField_2);
		
		JButton btnNewButton_2 = new JButton(" ... ");
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_2, 74, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, textField_2, -3, SpringLayout.WEST, btnNewButton_2);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton_2, -10, SpringLayout.EAST, frame.getContentPane());
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				path=URLDecoder.decode(new DirectoryChooser(pathDef,"ppt").selectedfile);

				textField_2.setText(path);
			}
		});
		frame.getContentPane().add(btnNewButton_2);
		
		
		btnNewButton_1 = new JButton("Import");
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton_1, 27, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton_1, -28, SpringLayout.SOUTH, frame.getContentPane());
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField_2.getText().equals(""))
				{
					System.out.println("Path null");
					JOptionPane.showMessageDialog(null, "Enter the presentation location", "", JOptionPane.INFORMATION_MESSAGE);
				}
				else
				{

					new ProgressDialog();
					System.out.println("setup done");

					
					frame.dispose();

				}
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		frame.getContentPane().add(btnNewButton_1);
		
	}
	}
	

