package gui;




import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import javax.swing.Timer;

import data.ConverttoDesktop;
import data.FFMPEGWrapper;
import SoundCapture.SoundCapture;
import Xuggler.DecodeAndSaveAudioVideo;
import Xuggler.VideoCapture;


public class Recording {
    SoundCapture sound,currentSound;
    VideoCapture muteVideo,currentMuteVideo;
    long startTime ;
    String audioPath,videoPath,outputPath,currentOpFileName,currentAuFileName,currentViFileName;
    public static boolean record=false;
    public Recording(){
        
		
    }
    
    class ProgressDialog extends JPanel
	implements ActionListener, 
	PropertyChangeListener{
		
		 /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JProgressBar progressBar;
		private JFrame innerFrame;
		private JPanel contentPane;
		 public Task task;
		 private JLabel lblNewLabel;
		 
		 class Task extends SwingWorker<Void, Void> {

			@Override
			protected Void doInBackground() throws Exception {
				int progress=0;
				 setProgress(0);
				 stopSlide();
			    setProgress(25);
			    Call.workspace.removeTimeline();
			    setProgress(60);
			    Call.workspace.populateTimeline();
				 setProgress(100);
				setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				innerFrame.dispose();
				return null;
				 
			}
		 }

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if ("progress" == evt.getPropertyName()) {
	            int progress = (Integer) evt.getNewValue();
	            progressBar.setIndeterminate(false);
	            progressBar.setValue(progress);
			}
			
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
		}
		ProgressDialog() {

			innerFrame = new JFrame("Stopping");
			innerFrame.setBounds(100, 100, 528, 299);
			
			progressBar = new JProgressBar(0, 100);
	        progressBar.setValue(0);
	        progressBar.setStringPainted(true); 
	        progressBar.setIndeterminate(true);
	      
	        JPanel panel = new JPanel();
	        panel.setLayout(new BorderLayout(0, 0));
	        panel.add(progressBar);
 
	        panel.setVisible(true);
	        panel.setOpaque(true);
	        innerFrame.setContentPane(panel);
	        
	        lblNewLabel = new JLabel("Stopping. Please wait....");
	        panel.add(lblNewLabel, BorderLayout.SOUTH);
	        innerFrame.setVisible(true);
	        System.out.println("Progress dialog created");
	        task = new Task();
	        task.addPropertyChangeListener(this);
	        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
	        task.execute();
		}
    
    }

    class ProgressDialog2 extends JPanel
   	implements ActionListener, 
   	PropertyChangeListener{
   		
   		 /**
   		 * 
   		 */
   		private static final long serialVersionUID = 1L;
   		private JProgressBar progressBar;
   		private JFrame innerFrame;
   		private JPanel contentPane;
   		 public Task task;
   		 private JLabel lblNewLabel;
   		 
   		 class Task extends SwingWorker<Void, Void> {

   			@Override
   			protected Void doInBackground() throws Exception {
   				int progress=0;
   				setProgress(0);
   				sound.stopRecording();
   		        muteVideo.stop();
   		        setProgress(20);
   		        File f2=new File(videoPath);
   		        File f1=new File(f2.getName());
   		        copyFile(f1,f2);
   		        setProgress(25);
   				f1.delete();
   		        long endTime=System.currentTimeMillis();
   		        long elapsedMilliSeconds = endTime - startTime;
   		        double elapsedSeconds = elapsedMilliSeconds / 1000.0;
   		        setProgress(45);
   		        try{
   		        Thread.sleep(1000);
   		        }
   		        catch(Exception e)
   		        {
   		            e.printStackTrace();
   		        }
   		        setProgress(60);
   		        DecodeAndSaveAudioVideo.stitch(videoPath, audioPath, outputPath);
   				setProgress(100);
   				setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
   				innerFrame.dispose();
   				return null;
   				 
   			}
   		 }

   		@Override
   		public void propertyChange(PropertyChangeEvent evt) {
   			if ("progress" == evt.getPropertyName()) {
   	            int progress = (Integer) evt.getNewValue();
   	            progressBar.setIndeterminate(false);
   	            progressBar.setValue(progress);
   			}
   			
   		}

   		@Override
   		public void actionPerformed(ActionEvent e) {
   			// TODO Auto-generated method stub
   			
   		}
   		ProgressDialog2() {

   			innerFrame = new JFrame("Stopping");
   			innerFrame.setBounds(100, 100, 528, 299);
   			
   			progressBar = new JProgressBar(0, 100);
   	        progressBar.setValue(0);
   	        progressBar.setStringPainted(true); 
   	        progressBar.setIndeterminate(true);
   	      
   	        JPanel panel = new JPanel();
   	        panel.setLayout(new BorderLayout(0, 0));
   	        panel.add(progressBar);
    
   	        panel.setVisible(true);
   	        panel.setOpaque(true);
   	        innerFrame.setContentPane(panel);
   	        
   	        lblNewLabel = new JLabel("Stopping. Please wait....");
   	        panel.add(lblNewLabel, BorderLayout.SOUTH);
   	        innerFrame.setVisible(true);
   	        System.out.println("Progress dialog created");
   	        task = new Task();
   	        task.addPropertyChangeListener(this);
   	        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
   	        task.execute();
   		}
       
       }
    
    
    class ProgressDialog3 extends JPanel
   	implements ActionListener, 
   	PropertyChangeListener{
   		
   		 /**
   		 * 
   		 */
   		private static final long serialVersionUID = 1L;
   		private JProgressBar progressBar;
   		private JFrame innerFrame;
   		private JPanel contentPane;
   		 public Task task;
   		 private JLabel lblNewLabel;
   		 
   		 class Task extends SwingWorker<Void, Void> {

   			@Override
   			protected Void doInBackground() throws Exception {
   				int progress=0;
   				 setProgress(0);
   				 decode();
   				convertMp4();
   				setProgress(25);
   				System.out.println("convert done.. ");
        		concatenateVideos();
        		System.out.println("concat done.. ");
        		setProgress(75);
        		Call.workspace.showOutput();  				 
   				 setProgress(100);
   				setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
   				innerFrame.dispose();
   				return null;
   				 
   			}
   		 }

   		@Override
   		public void propertyChange(PropertyChangeEvent evt) {
   			if ("progress" == evt.getPropertyName()) {
   	            int progress = (Integer) evt.getNewValue();
   	            progressBar.setIndeterminate(false);
   	            progressBar.setValue(progress);
   			}
   			
   		}

   		@Override
   		public void actionPerformed(ActionEvent e) {
   			// TODO Auto-generated method stub
   			
   		}
   		ProgressDialog3() {

   			innerFrame = new JFrame("Stitching in progress");
   			innerFrame.setBounds(100, 100, 528, 299);
   			
   			progressBar = new JProgressBar(0, 100);
   	        progressBar.setValue(0);
   	        progressBar.setStringPainted(true); 
   	        progressBar.setIndeterminate(true);
   	      
   	        JPanel panel = new JPanel();
   	        panel.setLayout(new BorderLayout(0, 0));
   	        panel.add(progressBar);
    
   	        panel.setVisible(true);
   	        panel.setOpaque(true);
   	        innerFrame.setContentPane(panel);
   	        
   	        lblNewLabel = new JLabel("Stitching in progress. Please wait....");
   	        panel.add(lblNewLabel, BorderLayout.SOUTH);
   	        innerFrame.setVisible(true);
   	        System.out.println("Progress dialog created");
   	        task = new Task();
   	        task.addPropertyChangeListener(this);
   	        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
   	        task.execute();
   		}
       
       }
    
    public static void copyFile( File from, File to ){
	    try {
			Files.copy( from.toPath(), to.toPath() );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    public void startContinuousRecording(){
        try {
			audioPath=new File(Call.workspace.audioPath,"recording.wav").getAbsolutePath();
			sound=new SoundCapture(audioPath);
			videoPath = new File(Call.workspace.tempPath,"recording.flv").getAbsolutePath();
			outputPath=new File(Call.workspace.outputPath,"continuous.flv").getAbsolutePath();
			muteVideo=new VideoCapture("recording.flv");
			sound.startRecording();
			muteVideo.start();
            startTime = System.currentTimeMillis();
          
        } catch (Exception ex) {
            Logger.getLogger(Recording.class.getName()).log(Level.SEVERE, null, ex);
        } 
       
    }
    
    
    public void stitch()
    {
    	/*Timer t = new Timer(1000 * 4, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // do your reoccuring task
            	
            	try {
            		convertMp4();
            		concatenateVideos();
            		Call.workspace.showOutput();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
        t.start(); 
        t.setRepeats(false);*/
    	new ProgressDialog3();
        
    }
    public void startSlide()
    {
    	//Set current recording paths
  
		String num=Call.workspace.currentSlide.getName();
		currentOpFileName=new File(Call.workspace.videosPath,(num+".flv")).getAbsolutePath();
		currentAuFileName=new File(Call.workspace.audioPath,(num+".wav")).getAbsolutePath();
		currentViFileName=new File(Call.workspace.tempPath,(num+".flv")).getAbsolutePath();

    	//Start recording on current paths
    	try {
			currentSound=new SoundCapture(currentAuFileName);
			String fname=new File(currentViFileName).getName();
			currentMuteVideo=new VideoCapture(fname);
			currentSound.startRecording();
			currentMuteVideo.start();
        } catch (Exception ex) {
      
            Logger.getLogger(Recording.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    public void stopSlide()
    {
    	currentSound.stopRecording();
        currentMuteVideo.stop();
        String fname=new File(currentViFileName).getName();
        File f1=new File(fname);
        File f2=new File(currentViFileName);
        copyFile(f1,f2);
		
        long endTime=System.currentTimeMillis();
        long elapsedMilliSeconds = endTime - startTime;
        double elapsedSeconds = elapsedMilliSeconds / 1000.0;
        try{
        Thread.sleep(1000);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        DecodeAndSaveAudioVideo.stitch(currentViFileName, currentAuFileName, currentOpFileName);
        f1.delete();
    }
    public void discardSlide()
    {
    	currentSound.stopRecording();
        currentMuteVideo.stop();
        String fname=new File(currentViFileName).getName();
        File f1=new File(fname);
        File f2=new File(currentViFileName);
        copyFile(f1,f2);
		f1.delete();
        long endTime=System.currentTimeMillis();
        long elapsedMilliSeconds = endTime - startTime;
        double elapsedSeconds = elapsedMilliSeconds / 1000.0;
        try{
        Thread.sleep(1000);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        DecodeAndSaveAudioVideo.stitch(currentViFileName, currentAuFileName, currentOpFileName);
        new File(currentAuFileName).delete();
        new File(currentViFileName).delete();
        new File(currentOpFileName).delete();
        Call.workspace.removeTimeline();
        Call.workspace.populateTimeline();
    }
    public void convertMp4()
    {
    	System.out.println("Started conversion");
    	File dir = new File(Call.workspace.videosPath);
    	File[] files = dir.listFiles(new FilenameFilter() {
    	    public boolean accept(File dir, String name) {
    	        return name.toLowerCase().endsWith(".flv");
    	    }
    	});
    	
    	for(int i=0;i<files.length;i++)
    	{
    		String f=files[i].getAbsolutePath().replace(".flv", ".mp4");
    		if(!(new File(f).exists()))
    		{
    			DecodeAndSaveAudioVideo.convertFormat(files[i].getAbsolutePath(), f);
    			System.out.println("Converting at "+f);
    		}
    	}
    }
    public void decode()
    {
    	int cnt=0;
    	for(String x: Call.workspace.Timeline)
    	{
    		if(x.contains("Slide")){
    		String fname=(x.replace("Slide=", "").replace("Video=", ""));
    		String awpath=new File(Call.workspace.audioPath,(fname+".wav")).getAbsolutePath();
    		System.out.println(awpath);
    		String ampath=new File(Call.workspace.audioPath,(fname+".mp3")).getAbsolutePath();
    		String vpath=new File(Call.workspace.videosPath,(fname+".mp4")).getAbsolutePath();
    		String tempPath=new File(Call.workspace.tempPath,"temp.mp4").getAbsolutePath();
    		File f=new File(vpath);
    		if(!f.exists())
    		{
    			if(new File(awpath).exists())
    			{
    				String imgPath=Call.workspace.librarySlides.get(cnt).getFile();
    				System.out.println(imgPath);
    				if(new File(imgPath).exists())
    					System.out.println("exists");
    				try {
    					FFMPEGWrapper ffm=new FFMPEGWrapper();
    					ffm.convertWavToMp3(awpath, ampath);
						ffm.stitchImageAndAudio(imgPath, ampath, vpath, tempPath);
					} catch (IOException | InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    			}
    		}
    		cnt++;
    		}
    	}
    			
    }
    public void concatenateVideos()
    {
    	ArrayList<String> vList=new ArrayList<String>();
    	String outp=Call.workspace.name+".mp4";
    	String fileoutput=new File(Call.workspace.outputPath,outp).getAbsolutePath();
    	for(String x: Call.workspace.Timeline)
    	{
    		String fname=(x.replace("Slide=", "").replace("Video=", ""))+".mp4";
    		File f=new File(Call.workspace.videosPath,fname);
    		if(f.exists())
    		{
    			fname=f.getAbsolutePath();
    			vList.add(fname);
    		}
    	}
    	System.out.println("stitiching going to start");
    	try {
			new FFMPEGWrapper().stitchVideo(vList, new File("concat.txt").getAbsolutePath(),fileoutput );
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public void concatenateVideos1()
    {
    	System.out.println("Started concatenation");
    	/*File dir = new File(Call.workspace.outputPath);
    	File[] files = dir.listFiles(new FilenameFilter() {
    	    public boolean accept(File dir, String name) {
    	        return name.toLowerCase().endsWith(".mp4");
    	    }
    	});*/
    	/*Wrong logic
    	 * for(int i=0;i<files.length;i++)
    	{
    		String f1,f2;
    		File temp1,temp;
    		temp=new File(Call.workspace.outputPath,"temp.mp4");
    		temp1=new File(Call.workspace.outputPath,"temp1.mp4");
    		
    	}*/
    	/*To be replicated
    	 * int cnt=0;
    	File f1 = null;
    	File temp1,temp;
		temp=new File(Call.workspace.outputPath,"temp.mp4");
		
    	for(Slide x: Call.workspace.librarySlides)
    	{
    		String fname=x.getName()+".mp4";
    		File f=new File(Call.workspace.outputPath,fname);
    		fname=f.getAbsolutePath();
    		if(f.exists())
    		{
    			if(cnt==0)
    			{
    				f1=new File(fname);
    				cnt++;
    			}
    			else if(cnt==1)
    			{
    				Xuggler.ConcatenateAudioAndVideo.concatenate(f1.getAbsolutePath(),f.getAbsolutePath() , temp.getAbsolutePath());
    				cnt++;
    			}
    			else
    			{
    				temp1=new File(Call.workspace.outputPath,"temp1.mp4");
    				Xuggler.ConcatenateAudioAndVideo.concatenate(temp.getAbsolutePath(),f.getAbsolutePath(),temp1.getAbsolutePath());
    				temp.delete();
    				
    				temp=new File(Call.workspace.outputPath,"temp.mp4");
    				temp1.renameTo(temp);
    				
    			}
    		}
    	}*/
    	int cnt=0;
    	File f1 = null;
    	File temp1,temp;
		temp=new File(Call.workspace.tempPath,"temp.mp4");
		
    	for(String x: Call.workspace.Timeline)
    	{
    		String fname=(x.replace("Slide=", "").replace("Video=", ""))+".mp4";
    		File f=new File(Call.workspace.videosPath,fname);
    		fname=f.getAbsolutePath();
    		if(f.exists())
    		{
    			if(cnt==0)
    			{
    				f1=new File(fname);
    				cnt++;
    			}
    			else if(cnt==1)
    			{
    				Xuggler.ConcatenateAudioAndVideo.concatenate(f1.getAbsolutePath(),f.getAbsolutePath() , temp.getAbsolutePath());
    				cnt++;
    			}
    			else
    			{
    				temp1=new File(Call.workspace.tempPath,"temp1.mp4");
    				Xuggler.ConcatenateAudioAndVideo.concatenate(temp.getAbsolutePath(),f.getAbsolutePath(),temp1.getAbsolutePath());
    				temp.delete();
    				
    				temp=new File(Call.workspace.tempPath,"temp.mp4");
    				temp1.renameTo(temp);
    				
    			}
    		}
    	}
    	String f=Call.workspace.name+".mp4";
    	String fileoutput=new File(Call.workspace.outputPath,f).getAbsolutePath();
    	File stitched=new File(fileoutput);
    	if(cnt==1)
    	{
    		copyFile(f1, stitched);
    	}
    	else
    	{
    		copyFile(temp, stitched);
    	}
    	
    	
    }
    public void startRecording()
    {
    	/*if(Call.workspace.continuous)
        	startContinuousRecording();*/
    	startSlide();
    }
    public void stopRecording()
    {
    	/*if(Call.workspace.continuous)
        	stopContinuousRecording();*/
    	
    	/*Timer t = new Timer(1000 * 4, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // do your reoccuring task
            	
            	try {
            		convertMp4();
            		concatenateVideos();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
        t.start(); 
        t.setRepeats(false);*/
      /*  stopSlide();
    	Call.workspace.removeTimeline();
    	Call.workspace.populateTimeline();*/
    	new ProgressDialog();
    }
    public void stopContinuousRecording(){
        sound.stopRecording();
        muteVideo.stop();
        File f1=new File("recording.flv");
        File f2=new File(videoPath);
        copyFile(f1,f2);
		f1.delete();
        long endTime=System.currentTimeMillis();
        long elapsedMilliSeconds = endTime - startTime;
        double elapsedSeconds = elapsedMilliSeconds / 1000.0;
        try{
        Thread.sleep(1000);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        DecodeAndSaveAudioVideo.stitch(videoPath, audioPath, outputPath);
    }
    public void pauseRecording()
    {
    	
    }
    public void unpauseRecording()
    {
    	
    }
}