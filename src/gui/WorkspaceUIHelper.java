package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

public class WorkspaceUIHelper {

	public static void disableAllRecordingToolkit()
	{
		Call.workspace.btnRecord.setEnabled(false);
		Call.workspace.btnDiscard.setEnabled(false);
		Call.workspace.btnNext.setEnabled(false);
		Call.workspace.stopbtn.setEnabled(false);
		Call.workspace.pauseButton.setEnabled(false);
		Timer t = new Timer(1000 * 4, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // do your reoccuring task
            	
            	try {
            		Call.workspace.btnRecord.setEnabled(true);
            		Call.workspace.btnDiscard.setEnabled(true);
            		Call.workspace.btnNext.setEnabled(true);
            		Call.workspace.stopbtn.setEnabled(true);
            		Call.workspace.pauseButton.setEnabled(true);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
        t.start(); 
        t.setRepeats(false);
	}

	public static void disableRecord()
	{
		Call.workspace.btnRecord.setEnabled(false);
		Call.workspace.btnDiscard.setEnabled(false);
		Call.workspace.btnNext.setEnabled(false);
		Call.workspace.stopbtn.setEnabled(false);
		Call.workspace.pauseButton.setEnabled(false);
		Timer t = new Timer(1000 * 4, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // do your reoccuring task
            	
            	try {
            		Call.workspace.stopbtn.setEnabled(true);
            		Call.workspace.pauseButton.setEnabled(true);
            		Call.workspace.btnNext.setEnabled(true);
            		Call.workspace.btnDiscard.setEnabled(true);
            		Call.workspace.btnRecord.setEnabled(false);
            		disableTimeline();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
        t.start(); 
        t.setRepeats(false);
	}
	public static void disableTimeline()
	{
		Call.workspace.btnStitch.setEnabled(false);
		Call.workspace.btnSwap.setEnabled(false);
		Call.workspace.btnSaveOrder.setEnabled(false);
		Call.workspace.btnDelete.setEnabled(false);
	}
	public static void enableTimeline()
	{
		Call.workspace.btnStitch.setEnabled(true);
		Call.workspace.btnSwap.setEnabled(true);
		//Call.workspace.btnSaveOrder.setEnabled(true);
		Call.workspace.btnDelete.setEnabled(true);
	}
	public static void disableStop()
	{
		Call.workspace.btnRecord.setEnabled(false);
		Call.workspace.btnDiscard.setEnabled(false);
		Call.workspace.btnNext.setEnabled(false);
		Call.workspace.stopbtn.setEnabled(false);
		Call.workspace.pauseButton.setEnabled(false);
		Timer t = new Timer(1000 * 4, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // do your reoccuring task
            	
            	try {
            		Call.workspace.stopbtn.setEnabled(false);
            		Call.workspace.pauseButton.setEnabled(false);
            		Call.workspace.btnNext.setEnabled(false);
            		Call.workspace.btnDiscard.setEnabled(false);
            		Call.workspace.btnRecord.setEnabled(true);
            		enableTimeline();
            		
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
        t.start(); 
        t.setRepeats(false);
	}
	
}
