package Xuggler;

import java.io.File;

import gui.Call;

public class VideoCapture implements Runnable
{
	Thread thread;
	String outFile;
	CaptureScreenToFile videoEncoder;
	public VideoCapture(String x)
	{
		outFile=x;
	}
	
    public void start() {
	  
      thread = new Thread(this);
      //thread.setName("Capture");
      thread.start();
    }

    public void stop() {
      thread = null;
      
      System.out.println("Stopping thread");
      
    }

    private void shutDown(String message) {
      if (message != null && thread != null) {
    	
        thread = null;
        
        System.err.println(message);
      }
    }
    
    public void run()
    {
      

      try
      {
    	videoEncoder = new CaptureScreenToFile(outFile);
        while (thread!=null)
        {
          System.out.println("encoded image");
          videoEncoder.encodeImage(videoEncoder.takeSingleSnapshot());

          try
          {
            // sleep for framerate milliseconds
            Thread.sleep(videoEncoder.getFps());
          }
          catch (InterruptedException e)
          {
           shutDown(e.getMessage());
          }
        }
        videoEncoder.closeStreams();
      }
      catch (RuntimeException e)
      {
      	shutDown(e.getMessage());
      }
    }
}