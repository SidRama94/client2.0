package data;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;

import gui.Call;

public class ConverttoAndroid {

	public static void copyFile( File from, File to ) {
		 //  Files.delete(to.toPath());
	    	
	    	try {
				Files.copy( from.toPath(), to.toPath() );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	public ConverttoAndroid(String location) {
		// TODO Auto-generated constructor stub
		File proj=new File(Call.workspace.tempPath,Call.workspace.name);
		proj.mkdir();
		
		File images=new File(proj.getAbsolutePath(),"images");
		images.mkdir();
		File theDir=new File(Call.workspace.imagesPath);
		for(File f:theDir.listFiles())
		{
			if(f.exists())
			{
				
				try {
					String fname=f.getName().replace("_", ".").replace("jpg", "png").replace("jpeg", "png");
					File fdest=new File(images.getAbsolutePath(),fname);
					BufferedImage bufferedImage = ImageIO.read(f);

					// write the bufferedImage back to outputFile
					ImageIO.write(bufferedImage, "png", fdest);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		
		File audio=new File(proj.getAbsolutePath(),"audio");
		audio.mkdir();
		theDir=new File(Call.workspace.audioPath);
		for(File f:theDir.listFiles())
		{
			if(f.exists())
			{
				String fname=f.getName().replace("_", ".");
				File fdest=new File(audio.getAbsolutePath(),fname);
				copyFile(f, fdest);
			}
		}
		
		File output=new File(proj.getAbsolutePath(),"output");
		output.mkdir();
		theDir=new File(Call.workspace.outputPath);
		for(File f:theDir.listFiles())
		{
			if(f.exists())
			{
				String fname=f.getName().replace("_", ".");
				File fdest=new File(output.getAbsolutePath(),fname);
				copyFile(f, fdest);
			}
		}
		new CreateZip(proj.getAbsolutePath(), location);
		try {
			FileUtils.deleteDirectory(proj);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
