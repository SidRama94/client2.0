package data;

import gui.Call;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;

import poi.ImagesToPptx;

public class ConverttoDesktop {

	public static void copyFile( File from, File to ) {
		 //  Files.delete(to.toPath());
	    	
	    	try {
				Files.copy( from.toPath(), to.toPath() );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	public ConverttoDesktop(String givenPath) {
		// TODO Auto-generated constructor stub
		
		String androidPath;
		 if(givenPath.contains(".zip"))
		 {
			 androidPath=new File(givenPath).getParent();
			 new ExtractZip(givenPath, androidPath);
			 androidPath=givenPath.replace(".zip", "");
		 }
		 else
			 androidPath=givenPath;
		ValidateAndroidProject v=new ValidateAndroidProject(androidPath);
		v.validate_name();
		Call.workspace.clean();
		String projName=new File(androidPath).getName(),fname,ext = ".jpg";
		File theDir=new File(androidPath,"images");
		int count =1;
		HashMap <Integer,File> map = new HashMap<Integer,File>();
		if(theDir.exists())
		{
			for(File file: theDir.listFiles())
			{
				int beginIndex=0,endIndex=0;
				beginIndex = file.getName().indexOf('.');
				endIndex = file.getName().lastIndexOf('.');
				System.out.println("BeginIndex: "+beginIndex);
				int index = Integer.parseInt(file.getName().substring(beginIndex+1, endIndex));
				System.out.println(index);
				map.put(index, file);
			}

			Map<Integer, File> treeMap = new TreeMap<Integer,File>(); /*(
			new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o2.compareTo(o1);
			}

		});*/
			treeMap.putAll(map);
			
			for (Map.Entry<Integer, File> entry : treeMap.entrySet()) {
				System.out.println("Key : " + entry.getKey() 
	                                      + " Value : " + entry.getValue().getName());
				fname = entry.getValue().getName();
				
				int i=fname.lastIndexOf(".");
				ext=fname.substring(i);
				String newstr=Call.workspace.name+"_"+count;
				File fimage=new File(Call.workspace.imagesPath,newstr+ext);
				copyFile(entry.getValue(), fimage);
				++count;	
			}
		}
		System.out.println("count: "+count);
		theDir=new File(androidPath,"audio");
		count=0;
		if(theDir.exists())
		{
			for(File file: theDir.listFiles())
			{
				fname=file.getName();
				String find=projName+".";
				String newstr=Call.workspace.name+"_";
				fname=fname.replace(find,newstr);
				File faudio=new File(Call.workspace.audioPath,fname);
				++count;
				copyFile(file, faudio);
				
			}
		}
		System.out.println("audio count: "+count);
		new ImagesToPptx(ext);
		
		Call.workspace.populateProject();
		if(givenPath.contains(".zip"))
		{	try {
				FileUtils.deleteDirectory(new File(androidPath));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}}
	}

}
