package data;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.IStatus;

public class ProjectCommunicationInstance  extends NanoHTTPD {

	public Project rootProject;
	
	int c;
	
	public ProjectCommunicationInstance(Project project)
	{
		super(8080);
		rootProject=project;
		try {
			start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	public static void launchInstance(final Project project)
	{
		
		Thread one = new Thread() {
		    public void run() {
		        new ProjectCommunicationInstance(project);
			    System.out.println( "\nRunning! Point your browers to http://localhost:8080/ \n" );
		    }  
		};
		one.start();
	}
	
	public static void main(String[] args) {
		Project p = new Project("assas",	"asassasa" );
		launchInstance(p);
	}

    @Override
    public Response serve(IHTTPSession session) {
        
        Map<String, String> parms = session.getParms();
        
        String root ;
        File temp = new File(rootProject.getProjectLocalURL(),"temp");
        File presentation = new File(rootProject.getProjectLocalURL(),"temp");
        File lokavidya = new File(rootProject.getProjectLocalURL(),"temp");
        File video = new File(rootProject.getProjectLocalURL(),"temp");
        File output= new File(rootProject.getProjectLocalURL(),"output");
        if (parms.get("temp") != null) {
        	root=temp.getAbsolutePath();
        }
        if (parms.get("video") != null) {
        	root=video.getAbsolutePath();
        }
        if (parms.get("lokavidya") != null) {
        	root=lokavidya.getAbsolutePath();
        }
        if (parms.get("presentation") != null) {
        	root=presentation.getAbsolutePath();
        }
        if (parms.get("output") != null) {
        	root=output.getAbsolutePath();
        }
        else {
           root=rootProject.getProjectLocalURL();
        }
        
        return newFixedLengthResponse(root);
    }
	
	 
	
}
