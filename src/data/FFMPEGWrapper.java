package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import com.profesorfalken.jpowershell.PowerShell;

class StreamGobbler extends Thread
{
    InputStream is;
    String type;
    
    StreamGobbler(InputStream is, String type)
    {
        this.is = is;
        this.type = type;
    }
    
    public void run()
    {
        try
        {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line=null;
            while ( (line = br.readLine()) != null)
                System.out.println(type + ">" + line);    
            } catch (IOException ioe)
              {
                ioe.printStackTrace();  
              }
    }
}
public class FFMPEGWrapper {

	String pathExecutable;
	
	public static void main(String args[])
	{
		FFMPEGWrapper wrapper= new FFMPEGWrapper();
		/*try {
			wrapper.convertWavToMp3("/home/frg/Documents/sample1.wav", "/home/frg/Documents/sample1.mp3");
			
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		ArrayList<String> vList=new ArrayList<String>();
		
		vList.add("/home/frg/Documents/blank.mp4");
		vList.add("/home/frg/Documents/Countdown.mp4");
		
		
		wrapper.standardizeResolution(vList);
		wrapper.standardizeFps(vList);
		try {
			wrapper.stitchVideo(vList, "/home/frg/Documents/temp.txt", "/home/frg/Documents/join.mp4");
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public FFMPEGWrapper()
	{
		String osname=System.getProperty("os.name");
		System.out.println(osname);

		if (osname.contains("Windows"))
		{
			System.out.println("Setting to Windows");
			pathExecutable = new File(new File(new File(new File("resources").getAbsolutePath(),"ffmpeg"),"bin").getAbsolutePath(),"ffmpeg.exe").getAbsolutePath();
		}
		else
		{

			System.out.println("Setting to Linux");
			//pathExecutable = new File(new File(sourcePath,"ffmpeg").getAbsolutePath(),"ffmpeg").getAbsolutePath();
			pathExecutable = new File(new File(new File("resources").getAbsolutePath(),"bin").getAbsolutePath(),"ffmpeg").getAbsolutePath();
			System.out.println(pathExecutable);
		}	
	}
	
	public boolean stitchImageAndAudio(String imgPath, String audioPath, String videoPath,String tempPath) throws IOException, InterruptedException
	{
		/*
		 * Check if final or temp exists delete if it does.
		 */
		System.out.println("stitching audio");
		File temp=  new File(tempPath);
		File video = new File(videoPath);
		if (temp.exists())
			temp.delete();
		if (video.exists())
			video.delete();
		String cmd =" "+pathExecutable;//+" -help";
		String cmdext= " -loop 1 -i "+imgPath+" -c:v libx264 -t 2 -pix_fmt yuv420p -vf scale=320:240 "+tempPath;
		cmd +=cmdext;
		System.out.println("The cmd for image stitching is: "+cmd);
		Runtime run;
		Process pr;
		BufferedReader buf;
		String line;
		String [] command = new String [] {
	
			pathExecutable,
			"-loop",
			"1",
			"-i",
			imgPath,
			"-c:v", "libx264", "-t",
			"2", "-pix_fmt", "yuv420p", "-vf", "scale=320:240",
			tempPath
		};
		try {
			System.out.println("Image part command: "+command);
			run = Runtime.getRuntime();
			pr = run.exec(command);
			System.out.println("The output for stitching the image part is: ");
			
/*//			buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
//			line = "";
//			while ((line=buf.readLine())!=null) {
//			System.out.println(line);
//			pr.waitFor();
//			System.out.println("Wait for has ended ");
//			
//			buf = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
//			line = "";
//			while ((line=buf.readLine())!=null) {
//				System.out.println(line);
//			}
//			pr.waitFor();
	*/		
			
			// any error message?
            StreamGobbler errorGobbler = new 
                StreamGobbler(pr.getErrorStream(), "ERROR");            
            
            // any output?
            StreamGobbler outputGobbler = new 
                StreamGobbler(pr.getInputStream(), "OUTPUT");
                
            // kick them off
            errorGobbler.start();
            outputGobbler.start();
                                    
            // any error???
            int exitVal = pr.waitFor();
            System.out.println("ExitValue: " + exitVal); 
			System.out.println("Wait for has ended ");
		
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		cmd =" "+pathExecutable;//+" -help";
		cmdext=" -i "+audioPath+" -i "+tempPath+" -c:a copy -vcodec copy -strict -2 "+videoPath;
		cmd +=cmdext;
		String [] commandAudio = new String [] {
				pathExecutable,
				"-i",
				audioPath,
				"-i",
				tempPath,
				"-c:a", 
				"copy",
				"-vcodec",
				"copy",
				videoPath
		};
		System.out.println("audio stitiching in progress");
		System.out.println("The cmd for audio stitching is: "+commandAudio);
		run = Runtime.getRuntime();
		System.out.println("The output for stitching the audio part is: ");
		pr = run.exec(commandAudio);
		pr.waitFor();
		System.out.println("AUDIO DONE!!!!!!");
		System.out.println("Wait for has ended ");
		buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		line = "";
		while ((line=buf.readLine())!=null) {
		System.out.println(line);
		}
		return true;
	}
	
	public boolean stitchVideo(List<String> videoPaths,String tempPath,String finalPath) throws IOException, InterruptedException
	{
		//Writing files to order.
		System.out.println(tempPath);
		System.out.println(finalPath);
        File orderVideoFile = new File(tempPath);
		try {
			FileOutputStream out = new FileOutputStream(orderVideoFile);
            for(int i=0;i<videoPaths.size();i++) {
                String data="file '"+videoPaths.get(i)+"'\n\r";
                out.write(data.getBytes());
                System.out.println(data);
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
		
		
		
		if(System.getProperty("os.name").contains("Windows"))
		{
			String cmd="& '" +pathExecutable+"'";//+" -help";
			String cmdext= " -f concat -i '"+orderVideoFile.getAbsolutePath()+"' -codec copy  '"+finalPath+"'";
			cmd+=cmdext;
			System.out.println("CMD WINDOWS: "+cmd);
			String [] command= new String[] {
					"& '",
					pathExecutable,
					"'",
					" -f concat -i '",
					orderVideoFile.getAbsolutePath(),
					"' -codec copy  '",
					finalPath,
					"'"
			};
			PowerShell powerShell = PowerShell.openSession();

			//Print results    
			System.out.println(powerShell.executeCommand(cmd).getCommandOutput());
		
			powerShell.close();
		}
		else
		{
			String[] command = new String[] {pathExecutable,
					"-f",
					"concat",
					"-i",
					orderVideoFile.getAbsolutePath(),
					"-codec",
					"copy",
					finalPath		
			};
			String cmd=pathExecutable;//+" -help";
			String cmdext= " -f concat -i "+orderVideoFile.getAbsolutePath()+" -codec copy  "+finalPath+"";
			cmd+=cmdext;
			cmd = URLDecoder.decode(cmd);
			System.out.println("CMD: "+command);
			Runtime run = Runtime.getRuntime();
			Process pr = run.exec(command);
			pr.waitFor();
			BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			String line = "";
			while ((line=buf.readLine())!=null) {
			System.out.println(line);
			}
			System.out.println("stitch done.. ");
		}
		
		return true;
		
	}
	public boolean standardizeResolution(List<String> videoPaths)
	{
		for(String vPath: videoPaths)
		{
			String cmd =" "+pathExecutable;//+" -help";
			String cmdext= " -i "+vPath+" -s 720x480 -b 512k -vcodec copy -acodec copy";
			cmd +=cmdext;
			String [] command = new String [] {
					pathExecutable,
					"-i",
					vPath,
					"-s", 
					"720x480",
					"-b",
					"512k",
					"-vcodec", 
					"copy",
					"-acodec",
					"copy"
			};
			Runtime run = Runtime.getRuntime();
			Process pr;
			try {
				pr = run.exec(command);
				pr.waitFor();
				BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				String line = "";
				while ((line=buf.readLine())!=null) {
				System.out.println(line);
				}
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		//ffmpeg -i <inputfilename> -s 640x480 -b 512k -vcodec mpeg1video -acodec copy
		return true;
	}
	
	public boolean standardizeFps(List<String> videoPaths)
	{
		for(String vPath: videoPaths)
		{
			String cmd =" "+pathExecutable;//+" -help";
			File f1=new File(new File(vPath).getParent(),"temp.mp4");
			String [] command = new String[] {
					pathExecutable,
					"-i",
					vPath,
					"-sameq",
					"-r",
					"4",
					"-y",
					f1.getAbsolutePath()
			};
			String cmdext= " -i "+vPath+" -sameq -r 4 -y "+f1.getAbsolutePath();
			cmd +=cmdext;
			Runtime run = Runtime.getRuntime();
			Process pr;
			try {
				pr = run.exec(command);
				pr.waitFor();
				BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				String line = "";
				while ((line=buf.readLine())!=null) {
				System.out.println(line);
				new File(vPath).delete();
				f1.renameTo(new File(vPath));
				}
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return true;
	}
	public  boolean convertWavToMp3(String wavPath, String mp3Path) throws IOException, InterruptedException
	{
		String [] command =   new String[] {
				pathExecutable,
				"-i",
				wavPath,
				"-f",
				"mp2",
				mp3Path
		};
		File file = new File(mp3Path);
		if(file.exists()) {
			file.delete();
		
		}
		String cmd =" "+pathExecutable;//+" -help";
		String cmdext= " -i "+wavPath+" -f mp2 "+mp3Path;
		cmd +=cmdext;
		Runtime run = Runtime.getRuntime();
		Process pr = run.exec(command);
		pr.waitFor();
		BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String line = "";
		while ((line=buf.readLine())!=null) {
		System.out.println(line);
		}
		return true;
	}
}
