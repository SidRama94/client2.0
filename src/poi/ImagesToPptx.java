package poi;

import gui.Call;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.sl.usermodel.PictureData;
import org.apache.poi.sl.usermodel.PictureData.PictureType;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFPictureData;
import org.apache.poi.xslf.usermodel.XSLFPictureShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;

import Dialogs.OpenAndroid;



public class ImagesToPptx {
	XMLSlideShow ppt;
	
	public ImagesToPptx(String ext) {
		try
		{
		ppt = new XMLSlideShow();
		ppt.setPageSize(new java.awt.Dimension(720, 540));
		int count =1;
		System.out.println("Project path: "+Call.workspace.path);
		File theDir=new File(Call.workspace.path,"images");
		String fname;
		if(theDir.exists())
		{
			for(File file: theDir.listFiles())
			{
				
				createSlideWithImage(file.getAbsolutePath(),ext);
				++count;
		}
		}

		savePpt();
		
		System.out.println("done");
		}
		catch(Exception e) {
			e.printStackTrace();
		}	
		
}
	
/*	Possible future use
 * private static BufferedImage resizeImage(BufferedImage originalImage, int type, int IMG_WIDTH, int IMG_HEIGHT) {
	    BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
	    Graphics2D g = resizedImage.createGraphics();
	    g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
	    g.dispose();

	    return resizedImage;
	}*/
	public void createSlideWithImage(String path, String ext) {
		try
		{

		XSLFSlide slide = ppt.createSlide();
		 /*BufferedImage originalImage = ImageIO.read(new File(path));
		 int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
		 BufferedImage resizeImageJpg = resizeImage(originalImage, type, 1024, 768);
	     ImageIO.write(resizeImageJpg, "png", new File(path));*/
		
		File image=new File(path);
		
		byte[] picture = IOUtils.toByteArray(new FileInputStream(image));
		PictureType type = PictureData.PictureType.PNG;
		if(ext.equals(".png"))
			type=PictureData.PictureType.PNG;
		else if(ext.contains(".jpg")||ext.contains(".jpeg"))
			type=PictureData.PictureType.JPEG;
		XSLFPictureData pd = ppt.addPicture(picture,type );
		XSLFPictureShape pic = slide.createPicture(pd);
		pic.setAnchor(new Rectangle(720,540));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void savePpt() {
		try {
			FileOutputStream out = new FileOutputStream(Call.workspace.pptPath);
			System.out.println(out.toString());
			
			ppt.write(out);
			out.close();
			Call.workspace.currentPptx = new XMLSlideShow(new FileInputStream(Call.workspace.pptPath));
	    	Call.workspace.currentPptSlide = Call.workspace.currentPptx.getSlides();
			System.out.println("done...");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String args[]) {
		new ImagesToPptx(".png");
	}
}
