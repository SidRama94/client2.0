# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "ASM_YASM"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_ASM_YASM
  "/home/frg/ffmpeg_sources/x265/source/common/x86/blockcopy8.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/blockcopy8.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/const-a.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/const-a.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/cpu-a.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/cpu-a.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/dct8.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/dct8.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/intrapred8.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/intrapred8.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/intrapred8_allangs.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/intrapred8_allangs.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/ipfilter8.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/ipfilter8.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/loopfilter.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/loopfilter.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/mc-a.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/mc-a.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/mc-a2.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/mc-a2.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/pixel-a.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/pixel-a.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/pixel-util8.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/pixel-util8.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/pixeladd8.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/pixeladd8.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/sad-a.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/sad-a.asm.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/ssd-a.asm" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/ssd-a.asm.o"
  )
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/frg/ffmpeg_sources/x265/source/common/bitstream.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/bitstream.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/common.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/common.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/constants.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/constants.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/cpu.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/cpu.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/cudata.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/cudata.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/dct.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/dct.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/deblock.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/deblock.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/frame.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/frame.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/framedata.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/framedata.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/intrapred.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/intrapred.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/ipfilter.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/ipfilter.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/loopfilter.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/loopfilter.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/lowres.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/lowres.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/md5.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/md5.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/param.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/param.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/piclist.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/piclist.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/picyuv.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/picyuv.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/pixel.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/pixel.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/predict.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/predict.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/primitives.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/primitives.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/quant.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/quant.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/scalinglist.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/scalinglist.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/shortyuv.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/shortyuv.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/slice.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/slice.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/threading.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/threading.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/threadpool.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/threadpool.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/vec/dct-sse3.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/vec/dct-sse3.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/vec/dct-sse41.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/vec/dct-sse41.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/vec/dct-ssse3.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/vec/dct-ssse3.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/vec/vec-primitives.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/vec/vec-primitives.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/version.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/version.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/wavefront.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/wavefront.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/x86/asm-primitives.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/x86/asm-primitives.cpp.o"
  "/home/frg/ffmpeg_sources/x265/source/common/yuv.cpp" "/home/frg/ffmpeg_sources/x265/build/linux/common/CMakeFiles/common.dir/yuv.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "EXPORT_C_API=1"
  "HAVE_INT_TYPES_H=1"
  "HIGH_BIT_DEPTH=0"
  "X265_ARCH_X86=1"
  "X265_DEPTH=8"
  "X265_NS=x265"
  "X86_64=1"
  "__STDC_LIMIT_MACROS=1"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/frg/ffmpeg_sources/x265/source/."
  "/home/frg/ffmpeg_sources/x265/source/common"
  "/home/frg/ffmpeg_sources/x265/source/encoder"
  "."
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
